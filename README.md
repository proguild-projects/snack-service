# Snack Service API

Serves data related to delicious snacks served in vending machines all over America!

## Endpoints

The base endpoint is `localhost:3000/api`.

| Resource      | Description                                                 | Method | Request                                                                          | Response (success)                                                                                                              | Response (error)                          |
|---------------|-------------------------------------------------------------|--------|----------------------------------------------------------------------------------|---------------------------------------------------------------------------------------------------------------------------------|-------------------------------------------|
| /snacks       | All snacks.                                                 | GET    | N/A                                                                              | ``` {   status: 200,   data: []  } ```                                                                                          | ``` {   status: 400,   details: {} }  ``` |
| /snacks       | Create a new snack.                                         | POST   | ``` {   name: 'Chocolate Donut',   price: 1.75,   image: 'choco-donut.jpg' } ``` | ``` {   status: 201,   data: {     id: 12,     name: 'Chocolate Donut',     price: 1.75,     image: 'choco-donut.jpg'   } } ``` | ``` {   status: 400,   details: {} }  ``` |
| /snacks/<:id> | Get a specific snack.                                       | GET    | Query params only: `https://tasty-snack.com/snacks/12`                           | ``` {   status: 200,   data: {     id: 12,     name: 'Chocolate Donut',     price: 1.75,     image: 'choco-donut.jpg'   } } ``` | ``` {   status: 400,   details: {}  } ``` |
| /snacks/<:id> | Update a specific snack.                                    | PUT    | ``` {   name: 'Chocolate Donut',   price: 3.00,   image: 'choco-donut.jpg' } ``` | ``` {   status: 200,   data: {     id: 12,     name: 'Chocolate Donut',     price: 3.00,     image: 'choco-donut.jpg'   } } ``` | ``` {   status: 400,   details: {}  } ``` |
| /snacks/<:id> | Update a specific snack with only fields that have changed. | PATCH  | ``` {   price: 3.00 } ```                                                        | ``` {   status: 200,   data: {     id: 12,     name: 'Chocolate Donut',     price: 3.00,     image: 'choco-donut.jpg'   } } ``` | ``` {   status: 400,   details: {}  } ``` |
| /snacks/<:id> | Delete a specific snack.                                    | DELETE | N/A                                                                              | 204                                                                                                                             | 400                                       |

