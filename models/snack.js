import snackData from './seed.json' assert { type: 'json' };

/** @type {Snack[]} */
var db = [];


export class Snack {
    /**
     * Snack
     * @constructor
     * @param {number} id
     * @param {string} name 
     * @param {number} price 
     * @param {number} quantity 
     * @param {string} image 
     */

    constructor({id, name, price, quantity, image}) {
        // TODO: Next, create long term datastore to manage ids and types
        this.id = id ? id : Math.floor(Math.random() * 1000 +1);
        this.name = name;
        this.price = price;
        this.quantity = quantity;
        this.image = image;
    }

    /** @returns {Snack[]} */
    static getAll() {
        return db;
    }

    /**
     * 
     * @param {number} snackId 
     * @returns {Snack}
     */
    static getById(snackId) {
        return db.filter(snack => snack.id === +snackId)[0];
    }

    /**
     * 
     * @param {number} snackId 
     * @param {object} snackData 
     * @returns {Snack}
     */
    static update(snackId, snackData) {
        let snack = Snack.getById(snackId);
        if (snack) {
            let updatedDb = db.filter(s => s.id !== +snackId);
            let updatedSnack = {
                ...snack,
                ...snackData
            };
            updatedDb.push(updatedSnack);
            db = updatedDb;

            return updatedSnack;
        }
    }

    /**
     * 
     * @param {object} snackData 
     * @returns {Snack}
     */
    static create(snackData) {
        snackData.id = Math.floor(Math.random() * 1000 +1);
        db.push(snackData);
        return snackData;
    }

    /**
     * 
     * @param {number} snackId 
     */
    static delete(snackId) {
        db = db.filter(snack => snack.id !== +snackId);
    }

    /**
     * Check the data has valid snack attributes.
     * @param {object} snackData 
     * @returns {boolean}
     */
    static isValid(snackData) {
        for (const attr in snackData) {
            if (!snackData[attr]) {
                return false;
            }
        }
        return true;
    }


    /**
     * Check the snack is already in the database.
     * @param {number} snackId 
     * @returns {boolean}
     */
    static exists(snackId) {
        let snack = Snack.getById(snackId);
        return snack ? true : false;
    }
}

db = snackData.map(snack => new Snack(snack));
