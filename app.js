import express from 'express';
import { Snack } from "./models/index.js";
import morgan from 'morgan';

const app = express();
const PORT = 3000;

// Middleware
app.use(express.json());  // req.body
app.use(morgan('tiny')); 


// Routes
app.route('/api/snacks')
    .get((req, res) => {
      res.json({ status: res.statusCode, data: Snack.getAll() });
    })
    .post((req, res) => {
      if (!Snack.isValid(req.body)) {
        res
          .status(400)
          .json({ status: 400, detail: 'Invalid data.'});
        return;
      } 
    let newSnack = Snack.create(req.body);
    res
      .status(201)
      .json({ status: res.statusCode, data: newSnack });
    })

app.route('/api/snacks/:snackId' )
    .get((req, res) => {
      let snack = Snack.getById(req.params.snackId);
      if (!snack) {
        res
          .status(400)
          .json({ status: res.statusCode, detail: 'Snack does not exist.'});
        return;
      }
      res.json({ status: res.statusCode, data: snack });
    })
    .put( (req, res) => {
      if (!Snack.exists(req.params.snackId) || !Snack.isValid(req.body)) {
        res
          .status(400)
          .json({ status: res.statusCode, detail: 'Invalid data.'});
        return;
      }
      let updatedSnack = Snack.update(req.params.snackId, req.body);
      res.json({ status: res.statusCode, data: updatedSnack });
    })
    .patch((req, res) => {
      if (!Snack.exists(req.params.snackId)) {
        res
          .status(400)
          .json({ status: res.statusCode, detail: 'Invalid data.'});
        return;
      } 
    let updatedSnack = Snack.update(req.params.snackId, req.body);
    res.json({ status: res.statusCode, data: updatedSnack })
    })
    .delete((req, res) => {
      let snack = Snack.getById(req.params.snackId);
      if (!snack) {
        res
          .status(400)
          .json({ status: res.statusCode, detail: 'Snack does not exist.'});
        return;
      } 
      Snack.delete(req.params.snackId);
      res
        .status(204)
        .json({ status: res.statusCode });
    })

app.listen(PORT, () => {
  console.log(`Snack Service listening on port ${PORT}`);
});

export default app;
